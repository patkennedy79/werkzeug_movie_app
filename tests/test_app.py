"""
This file (test_app.py) contains the functional tests for *app.py*.
"""


def test_get_homepage(test_client):
    """
    GIVEN a Werkzeug test client wrapping a MovieApp object
    WHEN the '/' page is requested (GET)
    THEN check that the response is valid
    """
    response = test_client.get('/')
    assert response.status_code == 200
    assert b'Werkzeug Movie App' in response.data


def test_post_homepage(test_client):
    """
    GIVEN a Werkzeug test client wrapping a MovieApp object
    WHEN the '/' page is posted to (POST)
    THEN check that a 405 (Method Not Allowed) error is returned
    """
    response = test_client.post('/')
    assert response.status_code == 405
    assert b'Werkzeug Movie App' not in response.data


def test_get_movies_page(test_client):
    """
    GIVEN a Werkzeug test client wrapping a MovieApp object
    WHEN the '/movies' page is requested (GET)
    THEN check that the response is valid
    """
    response = test_client.get('/movies')
    assert response.status_code == 200
    assert b'Werkzeug Movie App' in response.data
    assert b'Index' in response.data
    assert b'Movie Title' in response.data


def test_post_movies_page(test_client):
    """
    GIVEN a Werkzeug test client wrapping a MovieApp object
    WHEN the '/movies' page is posted to (POST)
    THEN check that a 405 (Method Not Allowed) error is returned
    """
    response = test_client.post('/movies')
    assert response.status_code == 405
    assert b'Werkzeug Movie App' not in response.data


def test_get_add_movie_page(test_client):
    """
    GIVEN a Werkzeug test client wrapping a MovieApp object
    WHEN the '/add' page is requested (GET)
    THEN check that the response is valid
    """
    response = test_client.get('/add')
    assert response.status_code == 200
    assert b'Werkzeug Movie App' in response.data
    assert b'Movie Title:' in response.data
    assert b'Submit' in response.data


def test_post_add_movie_page(test_client):
    """
    GIVEN a Werkzeug test client wrapping a MovieApp object
    WHEN the '/add' page is posted to (POST) with valid data
    THEN check that the response is valid
    """
    response = test_client.post('/add', data={'title': 'Cars 3'})
    assert response.status_code == 302  # Redirect


def test_get_default_movies(test_client, add_default_movies):
    """
    GIVEN a Werkzeug test client wrapping a MovieApp object and the default
          set of movies added to the data
    WHEN the '/movies' page is requested (GET)
    THEN check that the response is valid and the default set of movies are listed
    """
    response = test_client.get('/movies')
    assert response.status_code == 200
    assert b'Werkzeug Movie App' in response.data
    assert b'Pirates of the Caribbean' in response.data
    assert b'Knives Out' in response.data
    assert b'The Incredibles 2' in response.data


def test_invalid_url(test_client):
    """
    GIVEN a Werkzeug test client wrapping a MovieApp object
    WHEN the '/get_movies' page is posted to (POST) with valid data
    THEN check that a 404 (Page Not Found) error is returned
    """
    response = test_client.get('/get_movies')
    assert response.status_code == 404
