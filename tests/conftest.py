import pytest
from app import create_app
from werkzeug.test import Client
from werkzeug.wrappers import Response
import redis
from fakeredis import FakeStrictRedis


##################
#### Fixtures ####
##################

@pytest.fixture(scope='function')
def test_client(monkeypatch):
    """Returns a test client to use for testing the request/responses for MovieApp."""

    # Monkeypatch the methods used in `redis` with the `fakeredis` equivalents
    fake_redis = FakeStrictRedis()
    fake_redis.flushall()
    monkeypatch.setattr(redis, 'StrictRedis', fake_redis)
    monkeypatch.setattr(redis.Redis, 'lrange', fake_redis.lrange)
    monkeypatch.setattr(redis.Redis, 'lpush', fake_redis.lpush)

    yield Client(create_app(), Response)


@pytest.fixture(scope='function')
def add_default_movies(test_client):
    """Adds a default set of movies to the data storage."""
    test_client.post('/add', data={'title': 'Pirates of the Caribbean'})
    test_client.post('/add', data={'title': 'Knives Out'})
    test_client.post('/add', data={'title': 'The Incredibles 2'})
