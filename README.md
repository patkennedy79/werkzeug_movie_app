## Overview

This project demonstrates how to use [Werkzeug](https://werkzeug.palletsprojects.com/en/1.0.x/) (key module of Flask) to build a web application.

The application stores your favorite movies in a Redis in-memory data store:

![Werkzeug Movie App](static/img/screenshot.png?raw=true "Werkzeug Movie App")

## Installation Instructions

Pull down the source code from this GitLab repository:

```sh
git clone git@gitlab.com:patkennedy79/werkzeug_movie_app.git
```

Create a new virtual environment:

```sh
$ cd werkzeug_movie_app
$ python3 -m venv venv
```

Activate the virtual environment:

```sh
$ source venv/bin/activate
```

Install the python packages in requirements.txt:

```sh
(venv) $ pip install -r requirements.txt
```

## Redis Database

This web application requires a Redis database.  The easiest way to work with Redis is via Docker (run these command in a separate window from the Flask application):

```sh
$ docker pull redis
$ docker run --name redis-werkzeug -d -p 6379:6379 redis
```

## Run the Development Server

In the top-level directory, run the Werkzeug development server to serve the application:

```sh
(venv) $ python app.py
```

Navigate to 'http://127.0.0.1:5000' to view the website!

## Key Python Modules Used

* **Werkzeug** - collection of libraries that can be used to create a WSGI (Web Server Gateway Interface) web application in Python.
* **Jinga2** - templating engine
* **redis** - Python interface to Redis data storage
* **pytest**: framework for testing Python projects
* **pytest-cov**: pytest extension for running coverage.py to check code coverage of tests
* **flake8**: static analysis tool
* **fakeredis**: Python implementation of redis API for testing purposes

This application is written using Python 3.11.

## Testing

To run all the tests:

```sh
(venv) $ python -m pytest -v
```

To check the code coverage of the tests:

```sh
(venv) $ python -m pytest --cov-report term-missing --cov=app
```
